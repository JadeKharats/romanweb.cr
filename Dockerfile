FROM crystallang/crystal:latest as builder
LABEL maintainer="Jade D. Kharats <jade.kharats@gmail.com>"
WORKDIR /src
COPY . .
RUN shards install --production
RUN shards build --production --static

FROM busybox
WORKDIR /app
COPY --from=builder /src/bin/romanweb /app/romanweb
ENTRYPOINT ["/app/romanweb"]
